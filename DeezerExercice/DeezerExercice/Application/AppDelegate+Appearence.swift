//
//  AppDelegate+Appearence.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc extension AppDelegate {
    func customizeNavigationAppearence() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UINavigationBar.appearance().barTintColor = .grayApp
        UINavigationBar.appearance().tintColor = .whiteText
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}
