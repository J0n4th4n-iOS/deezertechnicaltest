//
//  ImageLoader.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

import UIKit

final class ImageLoader {
    static let shared = ImageLoader()
    private var imageCache: ImageCacheProtocol = ImageCache()
    
    func loadImage(from url: URL, completionHandler: @escaping ((UIImage?) -> Void)) {
        // first check in cache
        if let image = imageCache[url] {
            completionHandler(image)
        } else {
            // Else download image and store in cache
            download(from: url) { [weak self] image in
                guard let image = image else {
                    completionHandler(nil)
                    return
                }
                self?.imageCache[url] = image
                completionHandler(image)
            }
        }
    }
}

extension ImageLoader {
    // MARK: Private methods
    private func download(from url: URL, completionHandler: ((UIImage?) -> Void)? = nil) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                completionHandler?(UIImage(data: data))
            } else {
                completionHandler?(nil)
            }
        }
    }
}
