//
//  DelayedWorkItem.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc protocol WorkItemProtocol {
    func perform(_ block: @escaping () -> Void)
}

@objc class WorkItem: NSObject, WorkItemProtocol {
    private var pendingRequestWorkItem: DispatchWorkItem?
    private var afterDelay: TimeInterval?

    init(afterDelay: TimeInterval? = nil) {
        self.afterDelay = afterDelay
    }

    func perform(_ block: @escaping () -> Void) {
        pendingRequestWorkItem?.cancel()
        let requestWorkItem = DispatchWorkItem(block: block)
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.guaranteeMainThread(after: afterDelay, work: requestWorkItem)
    }
}
