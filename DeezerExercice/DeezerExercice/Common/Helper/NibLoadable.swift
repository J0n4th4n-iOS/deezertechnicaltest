//
//  NibLoadable.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import UIKit

public protocol NibLoadable {}

public extension NibLoadable where Self: UIView {
    
    func instantiateFromNib() -> UIView? {
        let nib = UINib(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }
    
    func loadNibContent() {
        guard let view = instantiateFromNib() else {
            return
        }
        view.showIn(view: self)
    }
}
