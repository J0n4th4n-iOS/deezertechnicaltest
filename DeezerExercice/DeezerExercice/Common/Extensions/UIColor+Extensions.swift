//
//  UIColor+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc extension UIColor {
    static let grayApp = UIColor(red: 44.0/255.0, green: 61.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    static let blueApp = UIColor(red: 17.0/255.0, green: 119.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    static let whiteText = UIColor(white: 1, alpha: 0.8)
}
