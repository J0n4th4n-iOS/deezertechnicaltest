//
//  UITableView+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

extension UITableView {

    //Variable-height UITableView tableHeaderView with autolayout
    func layoutTableViewHeader(with height: CGFloat? = nil) {
        guard let headerView = tableHeaderView else { return }
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()

        var frame = headerView.frame
        frame.size.height = height ?? headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        headerView.frame = frame

        tableHeaderView = headerView
    }

}
