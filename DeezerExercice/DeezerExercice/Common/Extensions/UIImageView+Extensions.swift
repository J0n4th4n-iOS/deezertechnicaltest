//
//  UIImageView+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 10/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc extension UIImageView {
    func load(from link: String?,
                  contentMode mode: ContentMode = .scaleAspectFit,
                  completionHandler: (() -> Void)? = nil) {
        guard let link = link, let url = URL(string: link) else { return }
        
        DispatchQueue.main.guaranteeMainThread { [weak self] in
            guard let self = self else { return }
            self.image = nil
            self.contentMode = mode
            ImageLoader.shared.loadImage(from: url) { [weak self] image in
                if let image = image {
                    DispatchQueue.main.guaranteeMainThread {
                        self?.image = image
                        completionHandler?()
                    }
                }
            }
        }
    }
}
