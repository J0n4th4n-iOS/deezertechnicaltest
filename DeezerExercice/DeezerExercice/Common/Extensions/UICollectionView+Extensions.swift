//
//  UICollectionView+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 14/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc enum CollectionViewState: NSInteger {
    case initialize, empty, error, loading, loaded
    
    var message: String? {
        switch self {
        case .initialize:
            return "initialize".localized
        case .empty:
            return "emptyMessage".localized
        case .error:
            return "errorMessage".localized
        case .loaded:
            return nil
        case .loading:
            return "loadingMessage".localized
        }
    }
    
    var image: UIImage? {
        switch self {
        case .empty, .loading, .initialize :
            return UIImage(named: "information")
        case .error :
            return UIImage(named: "error")
        case .loaded:
            return nil
        }
    }
}

@objc extension UICollectionView {
    func setBackgroundView(with state: CollectionViewState) {
        switch state {
        case .empty, .error, .loading, .initialize:
            self.showBackgroundView(forState: state)
        case .loaded:
            self.restore()
        }
        self.reloadData()
    }
    
    func restore() {
        self.backgroundView = nil
    }
}

extension UICollectionView {
    private func showBackgroundView(forState state: CollectionViewState) {
        let messageLabel = UILabel()
        messageLabel.numberOfLines = .zero
        messageLabel.font = UIFont.systemFont(ofSize: 17)
        messageLabel.textColor = .blueApp
        messageLabel.textAlignment = .center
        messageLabel.text = state.message

        let messageStackView = UIStackView(arrangedSubviews: [messageLabel])
        messageStackView.axis = .horizontal
        messageStackView.alignment = .bottom
        messageStackView.distribution = .fill
        messageStackView.backgroundColor = .clear
        
        let imageView = UIImageView(image: state.image)

        let imageStackView = UIStackView(arrangedSubviews: [imageView])
        imageStackView.axis = .horizontal
        imageStackView.alignment = .top
        imageStackView.distribution = .fill
        imageStackView.backgroundColor = .clear
        
        let stackView = UIStackView(arrangedSubviews: [messageStackView, imageStackView])
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .grayApp
        
        backgroundView = stackView
    }
}

