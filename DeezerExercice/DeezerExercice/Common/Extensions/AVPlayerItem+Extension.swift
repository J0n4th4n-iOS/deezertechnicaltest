//
//  AVPlayerItem+Extension.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import AVFoundation

extension AVPlayerItem {
    static func prepareItemWithFadeInOutEffect(_ url: URL) -> AVPlayerItem? {
        let asset = AVAsset(url: url)

        let duration = asset.duration
        let roundedDurationInSeconds = roundf(Float(CMTimeGetSeconds(duration)))
        
        let item = AVPlayerItem(asset: asset)

        guard let track = asset.tracks.first else { return nil }
        let inputParameters = AVMutableAudioMixInputParameters(track: track)

        let firstSecond = CMTimeRangeMake(start: CMTimeMakeWithSeconds(0, preferredTimescale: 1), duration: CMTimeMakeWithSeconds(1, preferredTimescale: 1))
        let lastSecond = CMTimeRangeMake(start: CMTimeMakeWithSeconds(Float64(roundedDurationInSeconds-1), preferredTimescale: 1), duration: CMTimeMakeWithSeconds(1, preferredTimescale: 1))

        inputParameters.setVolumeRamp(fromStartVolume: 0, toEndVolume: 0.8, timeRange: firstSecond)
        inputParameters.setVolumeRamp(fromStartVolume: 0.8, toEndVolume: 0, timeRange: lastSecond)

        let mix = AVMutableAudioMix()
        mix.inputParameters = [inputParameters]
        item.audioMix = mix
        return item
    }
}
