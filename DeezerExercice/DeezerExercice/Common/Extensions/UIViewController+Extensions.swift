//
//  File.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    // MARK: properties
    static var identifier: String {
        return String(describing: self)
    }
    
    // MARK: Viewcontroller Loaders
    static func load<T: UIViewController>(from storyboard: StoryboardIdentifier,
                                          withIdentifier identifier: String = T.identifier,
                                          inBundle bundle: Bundle = Bundle(for: T.self)) -> T? {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: bundle)
        let vc = storyboard.instantiateViewController(withIdentifier: identifier)
        return vc as? T
    }
}
