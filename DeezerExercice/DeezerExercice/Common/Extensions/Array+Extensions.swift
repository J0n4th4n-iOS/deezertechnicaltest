//
//  Array+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 07/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
extension Array {
    func getOrNull(_ index: Int) -> Element? {
        guard index >= .zero, index < endIndex else {
            return nil
        }
        return self[index]
    }
}
