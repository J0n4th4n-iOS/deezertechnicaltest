//
//  DispatchQueue+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

extension DispatchQueue {
    func guaranteeMainThread(after: TimeInterval? = nil, work: @escaping () -> Void) {
        if let after = after {
            DispatchQueue.main.asyncAfter(deadline: .now() + after) {
                work()
            }
        } else if Thread.isMainThread {
            work()
        } else {
            DispatchQueue.main.async {
                work()
            }
        }
    }
    
    func guaranteeMainThread(after: TimeInterval? = nil, work: DispatchWorkItem) {
        if let after = after {
            DispatchQueue.main.asyncAfter(deadline: .now() + after, execute: work)
        } else if Thread.isMainThread {
            work.perform()
        } else {
            DispatchQueue.main.async {
                work.perform()
            }
        }
    }
}
