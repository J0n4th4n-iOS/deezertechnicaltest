//
//  String+Extension.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 07/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        NSLocalizedString(self, tableName: "Strings", comment: "")
    }
}
