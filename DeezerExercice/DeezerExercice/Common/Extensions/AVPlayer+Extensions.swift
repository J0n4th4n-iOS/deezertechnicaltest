//
//  AVPlayer+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import AVFoundation

extension AVPlayer {
    var isPlaying:Bool {
        get {
            self.rate != .zero && self.error == nil
        }
     }
}
