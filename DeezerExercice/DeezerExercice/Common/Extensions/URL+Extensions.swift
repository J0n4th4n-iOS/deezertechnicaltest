//
//  URL+Extensions.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

extension URL {
    mutating func append(path: String? = nil, queryParameters: QueryParameters?) {
        guard var urlComponents = URLComponents(string: absoluteString) else { return }
        
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        queryParameters?.forEach {
            queryItems.append(URLQueryItem(name: $0, value: $1))
        }
        
        urlComponents.queryItems = queryItems
        if let finalUrl = urlComponents.url {
            self = finalUrl
        }
    }
}
