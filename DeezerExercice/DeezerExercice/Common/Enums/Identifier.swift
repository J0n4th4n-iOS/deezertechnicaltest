//
//  StoryboardIdentifier.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

enum StoryboardIdentifier: String {
    case main = "Main"
}

enum CellIdentifier: String {
    case artistCollectionViewCellIdentifier = "ArtistCollectionViewCellIdentifier"
    case songCellIdentifier = "SongCellIdentifier"
}

enum HeaderIdentifier: String {
    case artistDetailsHeaderView = "ArtistDetailsHeaderView"
}
