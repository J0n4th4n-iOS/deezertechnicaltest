//
//  Search.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

typealias QueryParameters = [String: String]

@objc enum SearchFilter: Int {
    case advancedArtist
    case album
    case artist
    
    func url(for searchtext: String?) -> URL? {
        guard var searchUrl = url else { return nil }
        
        searchUrl.append(queryParameters: queryParameters(for: searchtext))
        return searchUrl
    }
    
    private var url: URL? {
        var urlString: String?
        switch self {
        case .advancedArtist:
            urlString = "https://api.deezer.com/search/"
        case .album:
            urlString = "https://api.deezer.com/search/album"
        case .artist:
            urlString = "https://api.deezer.com/search/artist"
        }
        
        guard let urlSearch = urlString, let url = URL(string: urlSearch)  else { return nil }
        return url
    }
    
    private func queryParameters(for searchText: String? = nil) -> QueryParameters? {
        guard let searchText = searchText else { return nil }
        
        switch self {
        case .artist, .album:
            return ["q": searchText]
        case .advancedArtist:
            return ["q": "artist:\"\(searchText)\""]
        }
    }
    
}
