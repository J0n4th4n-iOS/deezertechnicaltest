//
//  Artists.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 07/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objcMembers
class Artist: NSObject, Codable {
    // MARK: Properties
    var artistIdentifier: Int?
    var artistName: String?
    var artistPictureUrl: String?
    var trackListUrl: String?
    
    private enum CodingKeys: String, CodingKey {
        case artistIdentifier = "id"
        case artistName = "name"
        case artistPictureUrl = "picture"
        case trackListUrl = "tracklist"
    }
}

@objc class Artists: NSObject, Codable {
    var items: [Artist]?
    var next: String?
    var total: Int?
    
    private enum CodingKeys: String, CodingKey {
        case items = "data"
        case total
        case next
    }
}
