//
//  Tracks.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class Track: Codable {
    var id: Int?
    var title: String?
    var previewUrl: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case previewUrl = "preview"
    }
}

class Tracks: Codable {
    var items: [Track]?
    
    private enum CodingKeys: String, CodingKey {
        case items = "data"
    }
}
