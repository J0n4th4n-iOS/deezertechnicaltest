//
//  Tracks.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class Album: Codable {
    var id: Int?
    var albumTitle: String?
    var coverUrl: String?
    var nbTracks: Int?
    var trackListUrl: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case albumTitle = "title"
        case coverUrl = "cover"
        case nbTracks = "nb_tracks"
        case trackListUrl = "tracklist"
    }
}

class Albums: Codable {
    var items: [Album]?
    // Don't use pagination here becaus we just want the first item so we skip catching "total" and "next" property
    
    private enum CodingKeys: String, CodingKey {
        case items = "data"
    }
}
