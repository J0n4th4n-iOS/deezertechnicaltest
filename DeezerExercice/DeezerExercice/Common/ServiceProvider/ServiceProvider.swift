//
//  ServiceProvider.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol ServiceProviderProtocol {
    func request<T: Decodable>(_ url: String?, completion: @escaping (Result<T>) -> Void)
    func request<T: Decodable>(_ endpoint: Endpoint, completion: @escaping (Result<T>) -> Void)
}

enum Endpoint {
    case search(searchFilter: SearchFilter, searchText: String)
    
    var url: URL? {
        switch self {
        case .search(let searchFilter, let searchText):
            guard let url = searchFilter.url(for: searchText) else { return nil }
            
            return url
        }
    }
}

enum ServiceError: Error {
    case missingData, wrongEndpoint
}

 enum Result<T> {
    case failure(Error), success(T)
}

@objc class ServiceProvider: NSObject {
    static let shared = ServiceProvider()
    let defaultSession = URLSession(configuration: .default)
}

extension ServiceProvider: ServiceProviderProtocol {
    // MARK: ServiceProviderProtocol
    func request<T: Decodable>(_ url: String?, completion: @escaping (Result<T>) -> Void) {
        guard let urlString = url else {
            completion(.failure(ServiceError.wrongEndpoint))
            return
        }
        
        executeRequest(with: URL(string: urlString), completion: completion)
    }
    
    func request<T: Decodable>(_ endpoint: Endpoint, completion: @escaping (Result<T>) -> Void) {
        executeRequest(with: endpoint.url, completion: completion)
    }
}

extension ServiceProvider {
    // MARK: Private methods
    private func executeRequest<T: Decodable>(with url: URL?, completion: @escaping (Result<T>) -> Void) {
        guard let url = url else {
            completion(.failure(ServiceError.wrongEndpoint))
            return
        }
        
        let dataTask = defaultSession.dataTask(with: url) { [weak self] data, _, error in
            if let error = error { completion(.failure(error)) }
            guard let self = self else { return }
            
            let result: Result<T> = self.deserializeJSON(with: data)
            completion(result)
        }
        dataTask.resume()
    }
    
    private func deserializeJSON<T: Decodable>(with data: Data?) -> Result<T> {
        guard let data = data else { return .failure(ServiceError.missingData) }
        
        do {
            let deserializedValue = try JSONDecoder().decode(T.self, from: data)
            return .success(deserializedValue)
        } catch let error {
            return .failure(error)
        }
    }
}

