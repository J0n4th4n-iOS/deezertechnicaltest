//
//  PlayerView.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import AVFoundation

typealias PlayerItem = (url: String?, titleSong: String?)

protocol PlayerViewProtocol {
    func loadSong(_ playerItem: PlayerItem?)
}

protocol PlayerViewDelegate: class{
    func nextSong()
}

enum SongState: Equatable {
    case load(playerItem: PlayerItem?)
    case play
    case pause
    case stop
    
    static func == (lhs: SongState, rhs: SongState) -> Bool {
        switch (lhs, rhs) {
        case (.load, .load), (.play, play), (.pause, .pause), (.stop, .stop):
            return true
        default:
            return false
        }
    }
}

class PlayerView: UIView, NibLoadable {
    weak var delegate: PlayerViewDelegate?
    
    // State system
    private var songState: SongState = .stop {
        didSet {
            switch songState {
            case .load(let playerItem):
                load(playerItem)
            case .play:
                play()
            case .pause:
                pause()
            case .stop:
                stop()
            }
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        loadNibContent()
        addObsserver()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNibContent()
        addObsserver()
    }
    
    // MARK: Outlets & properties
    @IBOutlet weak var titleSongLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    
    private var playerItem: AVPlayerItem?
    
    private lazy var playerQueue : AVQueuePlayer = {
        let queuePlayer = AVQueuePlayer()
        
        return AVQueuePlayer()
    }()
    
    // MARK: view lifecycle
    deinit {
        NotificationCenter.default.removeObserver(NSNotification.Name.AVPlayerItemDidPlayToEndTime)
    }
    
    // MARK: Actions
    @IBAction func onPlayPauseButton(_ sender: Any) {
        songState = playerQueue.isPlaying ? .pause : .play
    }
    
    @IBAction func onNextButton(_ sender: Any) {
        delegate?.nextSong()
    }
}

extension PlayerView: PlayerViewProtocol {
    func loadSong(_ playerItem: PlayerItem?) {
        songState = .load(playerItem: playerItem)
    }
}

extension PlayerView {
    private func addObsserver() {
        // Observe AVPlayerItem
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    private func load(_ item: PlayerItem?) {
        titleSongLabel.text = item?.1 ?? "stop".localized
        
        guard
            let urlString = item?.0,
            let previewUrl = URL(string: urlString),
            let preparedItem = AVPlayerItem.prepareItemWithFadeInOutEffect(previewUrl)
        else {
            songState = .stop
            return
        }

        playerItem = preparedItem
        
        playerQueue.removeAllItems()
        playerQueue.insert(preparedItem, after: nil)
        playerQueue.play()
        configureButtons(isPlaying: true)
    }
    
    private func play() {
        playerQueue.play()
        configureButtons(isPlaying: true)
    }
    
    private func pause() {
        playerQueue.pause()
        configureButtons(isPlaying: false)
    }
    
    private func stop() {
        playerQueue.removeAllItems()
        configureButtons(isPlaying: false)
    }
    
    private func configureButtons(isPlaying: Bool) {
        playPauseButton.isEnabled = songState != .stop
        nextButton.isEnabled = playPauseButton.isEnabled
        isPlaying ? playPauseButton.setImage(UIImage(named: "pause"), for: .normal) : playPauseButton.setImage(UIImage(named: "play"), for: .normal)
        
    }
    
    @objc func playerDidFinishPlaying(sender: Notification) {
        // Your code here
        delegate?.nextSong()
    }
}
