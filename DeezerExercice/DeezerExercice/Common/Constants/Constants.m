//
//  Constants.m
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Numbers
// Numbers
double const CollectionViewInset = 16.0;
double const CollectionTileWidth = 120.0;
double const StaticCellHeight = 24;

#pragma mark - Identifier
NSString *const ArtistCollectionViewCellIdentifier = @"ArtistCollectionViewCellIdentifier";
NSString *const ShowDetailsArtistSegueIdentifier = @"showDetailsArtistSegueIdentifier";
