//
//  Constants.h
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

extern double const CollectionViewInset;
extern double const CollectionTileWidth;
extern double const StaticCellHeight;

extern NSString *const ArtistCollectionViewCellIdentifier;
extern NSString *const ShowDetailsArtistSegueIdentifier;

#endif /* Constants_h */
