//
//  Constants.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 13/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

struct Constants {
    struct Size {
        static let artistImageSize: Int = 120;
    }
    
    struct EdgeInset {
        static let topBottomCollectionViewInset: Int = 16
    }
}
