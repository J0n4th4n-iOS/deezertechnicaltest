//
//  DRZArtistSearchManager.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc class ArtistSearchManager: NSObject, ArtistSearchManagerProtocol {
    // MARK: Properties
    private let serviceProvider: ServiceProviderProtocol
    
    init(with serviceProvider: ServiceProviderProtocol) {
        self.serviceProvider = serviceProvider
    }
    
    private var isFetchInProgress = false
    
    func search(type: SearchFilter, text: String,
                success: @escaping (Artists) -> Void,
                failure: @escaping (Error) -> Void) {
        
        serviceProvider.request(.search(searchFilter: type, searchText: text)) { [weak self] (results: Result<Artists>) in
            self?.processingSearchResults(results, success: success, failure: failure)
        }
    }
    
    func loadNextArtists(url: String?,
                         success: @escaping (Artists) -> Void,
                         failure: @escaping (Error) -> Void) {
        guard !isFetchInProgress else { return }
        
        isFetchInProgress = true
        serviceProvider.request(url) { [weak self] (results: Result<Artists>) in
            guard let self = self else { return }
            self.isFetchInProgress = false
            self.processingSearchResults(results, success: success, failure: failure)
        }
    }
    
    func cancel() {
        isFetchInProgress = false
    }
}

extension ArtistSearchManager {
    // MARK: Private methods
    func processingSearchResults(_ results: Result<Artists>,
                                 success: @escaping (Artists) -> Void,
                                 failure: @escaping (Error) -> Void) {
        switch results {
        case .success(let artists):
            success(artists)
        case .failure(let error):
            failure(error)
        }
    }
}
