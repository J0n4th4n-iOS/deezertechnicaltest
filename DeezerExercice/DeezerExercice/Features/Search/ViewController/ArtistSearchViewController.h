//
//  DZRArtistSearchViewController.h
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ArtistSearchPresenterProtocol;

@interface ArtistSearchViewController: UIViewController
    @property (nonatomic, strong) UISearchController *searchController;
    @property (nonatomic, strong) id<ArtistSearchPresenterProtocol> presenter;
    @property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@end
