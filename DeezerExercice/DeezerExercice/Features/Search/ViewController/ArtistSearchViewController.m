//
//  DZRArtistSearchViewController.m
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import "ArtistSearchViewController.h"
#import "ArtistCollectionViewCell.h"
#import "DeezerExercice-Swift.h"
#import "Constants.h"

@interface ArtistSearchViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDataSourcePrefetching, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>
@end

@implementation ArtistSearchViewController
#pragma mark - init
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self assemble];
    }
    return self;
}

#pragma mark - view lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.presenter numberOfRows];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ArtistCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ArtistCollectionViewCellIdentifier forIndexPath:indexPath];
    [self.presenter configure:cell at:indexPath.row];

    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.presenter didSelectItemAt:indexPath.row];
}

#pragma mark - UICollectionViewDataSourcePrefetching
- (void)collectionView:(UICollectionView *)collectionView prefetchItemsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
    [self.presenter shouldPrefetchItemsAt:indexPaths];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(CollectionViewInset, CollectionViewInset, CollectionViewInset, CollectionViewInset);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Show three items by line
    double widthTile = self.collectionView.frame.size.width/3 - (2*CollectionViewInset);
    return CGSizeMake(widthTile, widthTile + StaticCellHeight);
}

#pragma mark - UISearchBarDelegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.presenter clearSearchResults];
    [self.collectionView setBackgroundViewWith:CollectionViewStateInitialize];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.presenter searchWithArtistName:searchText];
}

#pragma mark - Private methods
- (void)setupUI {
    self.collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.collectionView setBackgroundViewWith:CollectionViewStateInitialize];
    [self buildSearchComponent];
}

- (void)buildSearchComponent {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchBar.delegate = self;
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    self.searchController.searchBar.placeholder = @"Artist";
    
    self.navigationItem.searchController = self.searchController;
    self.navigationItem.hidesSearchBarWhenScrolling = NO;
    self.definesPresentationContext = YES;
}

- (NSArray<NSIndexPath *> *)visibleIndexPathsToReloadWithIntersecting:(NSArray<NSIndexPath *> *)indexPaths {
    NSMutableSet *indexPathForVisibleItems = [NSMutableSet setWithArray:self.collectionView.indexPathsForVisibleItems];
    NSSet *indexPathsToReload = [NSSet setWithArray:indexPaths];
    [indexPathForVisibleItems intersectSet:indexPathsToReload];
    return indexPathForVisibleItems.allObjects;
}

@end
