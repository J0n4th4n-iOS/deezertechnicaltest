//
//  ArtistSearchViewController+SearchViewProtocol.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc extension ArtistSearchViewController: ArtistSearchViewProtocol {
    func showBackgroundView(for state: CollectionViewState) {
        DispatchQueue.main.guaranteeMainThread { [weak self] in
            self?.collectionView.setBackgroundView(with: state)
        }
    }

    func reloadData(at indexPaths: [IndexPath]?) {
        DispatchQueue.main.guaranteeMainThread { [weak self] in
            guard let self = self else { return }
            self.collectionView.setBackgroundView(with: .loaded)
            indexPaths == nil ? self.collectionView.reloadData() : self.collectionView.reloadItems(at: indexPaths ?? [])
        }
    }
}
   
@objc extension ArtistSearchViewController {
    func assemble() {
        self.presenter = ArtistSearchPresenter(with: self, manager: ArtistSearchManager(with: ServiceProvider.shared), workItem: WorkItem(afterDelay: 0.5))
    }
}
