//
//  DZRArtistCollectionViewCell.h
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Artist;

@protocol ArtistCollectionViewCellProtocol
    - (void)configureWithArtist:(Artist * _Nullable)artist;
@end

@interface ArtistCollectionViewCell: UICollectionViewCell <ArtistCollectionViewCellProtocol>
@end
