//
//  DZRArtistCollectionViewCell.m
//  DeezerExercice
//  Copyright (c) 2015 Deezer. All rights reserved.
//

#import "ArtistCollectionViewCell.h"
#import "DeezerExercice-Swift.h"

@interface ArtistCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView * _Nullable artistImage;
@property (weak, nonatomic) IBOutlet UILabel * _Nullable artistName;

@end

@implementation ArtistCollectionViewCell

#pragma mark - view lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

#pragma mark - ArtistCollectionViewCellProtocol
- (void)configureWithArtist:( Artist * _Nullable )artist {
    artist == nil ? [self.activityIndicator startAnimating] : [self.activityIndicator stopAnimating];
    [self restore];
    
    [self.artistImage loadFrom:artist.artistPictureUrl contentMode:UIViewContentModeScaleAspectFit completionHandler:^{
        self.artistName.text = artist.artistName;
        self.artistImage.cornerRadius = self.artistImage.frame.size.width/2;
        [self.artistName setHidden:NO];
        [self.artistImage setHidden:NO];
    }];
}

#pragma mark - Private methods
- (void) setupUI {
    self.activityIndicator.hidesWhenStopped = true;
    
    [self.artistImage setHidden: YES];
    [self.artistName setHidden: YES];
    self.artistImage.borderWidth = 2;
    self.artistImage.borderColor = UIColor.whiteColor;
}

- (void) restore {
    self.artistImage.image = nil;
    [self.artistImage setHidden:YES];
    self.artistName.text = nil;
    [self.artistName setHidden:YES];
}
@end
