//
//  ArtistSearchProtocol.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc protocol ArtistSearchViewProtocol: class {
    func showBackgroundView(for state: CollectionViewState)
    func reloadData(at indexPaths: [IndexPath]?)
}

@objc protocol ArtistSearchPresenterProtocol {
    func numberOfRows() -> Int
    func configure(_ cell: ArtistCollectionViewCellProtocol, at index: Int)
    func didSelectItem(at index: Int)
    func shouldPrefetchItems(at indexPaths: [IndexPath])
    func search(artistName: String?)
    func clearSearchResults()
}

@objc protocol ArtistSearchManagerProtocol {
    func search(type: SearchFilter,
                text: String,
                success: @escaping (Artists) -> Void,
                failure: @escaping (Error) -> Void)
    
    func loadNextArtists(url: String?,
                         success: @escaping (Artists) -> Void,
                         failure: @escaping (Error) -> Void)
    
    func cancel()
}
