//
//  DRZArtistSearchPresenter.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

@objc class ArtistSearchPresenter: NSObject {
    // MARK: Properties
    private weak var view: (ArtistSearchViewProtocol & UIViewController)?
    private let manager: ArtistSearchManagerProtocol
    private let workItem: WorkItemProtocol
    
    private var artists: Artists?
    
    // MARK: Init
    @objc init(with view: (ArtistSearchViewProtocol & UIViewController), manager: ArtistSearchManagerProtocol, workItem: WorkItemProtocol) {
        self.view = view
        self.manager = manager
        self.workItem = workItem
    }
}

extension ArtistSearchPresenter: ArtistSearchPresenterProtocol {
    // MARK: ArtistSearchPresenterProtocol
    func numberOfRows() -> Int {
        artists?.total ?? .zero
    }
    
    func configure(_ cell: ArtistCollectionViewCellProtocol, at index: Int) {
        guard let artist = artists?.items?.getOrNull(index) else {
            cell.configure(with: .none)
            return
        }
        cell.configure(with: artist)
    }
    
    func didSelectItem(at index: Int) {
        guard let view = view, let artist = artists?.items?.getOrNull(index) else { return }
        Router.showFeature(.details(artist: artist), from: view)
    }
    
    func shouldPrefetchItems(at indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            manager.loadNextArtists(url: artists?.next) { [weak self] nextArtists in
                // Append
                guard let self = self, let nextArtists = nextArtists.items else { return }
                self.view?.reloadData(at: self.calculateIndexPathsToReload(from: nextArtists))
                self.artists?.items?.append(contentsOf: nextArtists)
            } failure: { [weak self] error in
                self?.view?.showBackgroundView(for: .error)
            }
        }
    }
    
    func search(artistName: String?) {
        workItem.perform { [weak self] in
            guard let self = self else { return }
            
            if let artistName = artistName, !artistName.isEmpty {
                self.manager.search(type: .artist, text: artistName) { [weak self] artists in
                    guard let self = self else { return }
                    self.artists = artists
                    artists.items?.isEmpty ?? true ? self.view?.showBackgroundView(for: .empty) : self.view?.reloadData(at: .none)
                } failure: { error in
                    self.view?.showBackgroundView(for: .error)
                }
            } else {
                self.clearSearchResults()
            }
        }
    }
    
    func clearSearchResults() {
        clearResults()
        self.view?.showBackgroundView(for: .initialize)
    }
}

extension ArtistSearchPresenter {
    // MARK: Private methods
    private func isLoadingCell(for indexPath: IndexPath) -> Bool {
        indexPath.row >= artists?.items?.count ?? .zero
    }
    
    private func calculateIndexPathsToReload(from newItems: [Artist]) -> [IndexPath] {
        guard let artists = artists else { return [] }
        let startIndex = artists.items?.count ?? .zero - newItems.count
        let endIndex = startIndex + newItems.count
        let indexPathsToReload = (startIndex..<endIndex).map { IndexPath(row: $0, section: .zero) }
        return indexPathsToReload
    }
    
    private func clearResults() {
        resetSearch()
        view?.reloadData(at: .none)
    }
    
    private func resetSearch() {
        artists = nil
        manager.cancel()
    }
}
