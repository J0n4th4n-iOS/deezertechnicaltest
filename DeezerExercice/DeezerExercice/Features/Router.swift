//
//  Router.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 06/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import UIKit

enum FeatureType {
    case details(artist: Artist)
}

class Router: NSObject {
    public static func showFeatureAsRoot(_ featureType: FeatureType,
                                         isNavigationControllerRequired: Bool = false,
                                         on window: UIWindow?) {
        let rootViewController = isNavigationControllerRequired ?
            UINavigationController(rootViewController: getAssembledFeature(featureType)) :
            getAssembledFeature(featureType)
        window?.rootViewController = rootViewController
    }
    
    public static func showFeature(_ featureType: FeatureType, from viewController: UIViewController) {
        let featureViewController =  getAssembledFeature(featureType)
        viewController.show(featureViewController, sender: nil)
    }
}

extension Router {
    // MARK: - Private methods
    private static func getAssembledFeature(_ featureType: FeatureType) -> UIViewController {
        var featureViewController: UIViewController?
        switch featureType {
        case .details(let artist):
            featureViewController = ArtistDetailsViewController.assembleModule(with: artist)
        break
        }
        
        guard let viewController = featureViewController else {
            fatalError("Feature error : feature has not been assembled")
        }
        return viewController
    }
}
