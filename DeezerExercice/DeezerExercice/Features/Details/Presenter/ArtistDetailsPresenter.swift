//
//  ArtistDetailsPresenter.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class ArtistDetailsPresenter {
    private weak var view: ArtistDetailsViewProtocol?
    private let manager: ArtistDetailsManagerProtocol
    
    private var artist: Artist?
    private var trackList: [Track]?
    private var currentSongIndex = -1
    
    // MARK: Init
    init(with view: ArtistDetailsViewProtocol, manager: ArtistDetailsManagerProtocol, artist: Artist? = nil) {
        self.view = view
        self.manager = manager
        self.artist = artist
    }
}

extension ArtistDetailsPresenter: ArtistDetailsPresenterProtocol {
    func didLoad() {
        manager.loadTracks(for: artist?.artistName, success: { [weak self] details in
            guard let self = self else { return }
            self.trackList = details.trackList
            DispatchQueue.main.guaranteeMainThread {
                self.view?.configure(with: details)
            }
        }, failure: { error in
            // Show error message on background view of tableview (same thing as collectionview
        })
    }
    
    func numberOfRows() -> Int {
        trackList?.count ?? .zero
    }
    
    func configure(_ cell: SongTableViewCellProtocol, at index: Int) {
        guard let song = trackList?.getOrNull(index) else { return }
        cell.configure(with: song.title)
    }
    
    func playSong(in player: PlayerViewProtocol, at index: Int) {
        currentSongIndex = index
        prepareSong(in: player)
    }
    
    func nextSong(in player: PlayerViewProtocol) {
        currentSongIndex += 1
        view?.selectRow(at: currentSongIndex)
        prepareSong(in: player)
    }
}

extension ArtistDetailsPresenter {
    // MARK: Private methods
    private func prepareSong(in player: PlayerViewProtocol) {
        let song = trackList?.getOrNull(currentSongIndex)
        player.loadSong((song?.previewUrl, titleSong: song?.title))
    }
}
