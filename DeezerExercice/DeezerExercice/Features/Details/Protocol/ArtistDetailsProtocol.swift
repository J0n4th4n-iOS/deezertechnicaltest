//
//  ArtistDetailsProtocol.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol ArtistDetailsViewProtocol: class {
    func configure(with modelUI: DetailsModelUI)
    func selectRow(at index: Int)
}

protocol ArtistDetailsPresenterProtocol {
    func didLoad()
    func numberOfRows() -> Int
    func configure(_ cell: SongTableViewCellProtocol, at index: Int)
    func playSong(in player: PlayerViewProtocol, at index: Int)
    func nextSong(in player: PlayerViewProtocol)
}

protocol ArtistDetailsManagerProtocol {
    func loadTracks(for artist: String?,
                    success: @escaping (DetailsModelUI) -> Void,
                    failure: @escaping (Error) -> Void)
}
