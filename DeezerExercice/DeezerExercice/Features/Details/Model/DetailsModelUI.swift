//
//  DetailsModelUI.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 12/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

struct DetailsModelUI {
    var albumCoverUrl: String?
    var albumTitle: String?
    var artistName: String?
    var trackList: [Track]?
}
