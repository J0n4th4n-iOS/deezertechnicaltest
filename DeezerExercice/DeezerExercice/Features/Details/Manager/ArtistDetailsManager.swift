//
//  ArtistDetailsManager.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class ArtistDetailsManager {
    // MARK: Properties
    private let serviceProvider: ServiceProviderProtocol
    
    init(with serviceProvider: ServiceProviderProtocol) {
        self.serviceProvider = serviceProvider
    }
}

extension ArtistDetailsManager: ArtistDetailsManagerProtocol {
    func loadTracks(for artist: String?,
                    success: @escaping (DetailsModelUI) -> Void,
                    failure: @escaping (Error) -> Void) {
        guard let artistName = artist, !artistName.isEmpty else { return }
        
        serviceProvider.request(.search(searchFilter: .album, searchText: artistName)) { [weak self] (albumResults: Result<Albums>) in
            guard let self = self else { return }
            switch albumResults {
            case .success(let albums):
                // Keep only the album with maximum of tracks
                albums.items?.sort { $0.nbTracks ?? .zero > $1.nbTracks ?? .zero }
                guard let album = albums.items?.first else { return }
               // Get tracklist for selected album
                self.serviceProvider.request(album.trackListUrl) { (trackListResults: Result<Tracks>) in
                    switch trackListResults {
                    case .success(let tracks):
                        let detailsModelUI = DetailsModelUI(albumCoverUrl: album.coverUrl, albumTitle: album.albumTitle, artistName: artist, trackList: tracks.items)
                        success(detailsModelUI)
                    case .failure(let error):
                        failure(error)
                    }
                }
            case .failure(let error):
                failure(error)
                break
            }
        }
    }
}
