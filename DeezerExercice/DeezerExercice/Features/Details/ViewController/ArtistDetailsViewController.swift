//
//  ArtistDetailsViewController.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

class ArtistDetailsViewController: UIViewController {
    // MARK: Outlets and properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var artistDetailsHeaderView: ArtistDetailsHeaderView!
    @IBOutlet weak var playerView: PlayerView!
    
    private var presenter: ArtistDetailsPresenterProtocol?
    
    // MARK: Assemble
    static func assembleModule(with artist: Artist) -> ArtistDetailsViewController? {
        guard let artistDetailsViewController: ArtistDetailsViewController = UIViewController.load(from: StoryboardIdentifier.main) else { return nil }
        
        artistDetailsViewController.presenter = ArtistDetailsPresenter(with: artistDetailsViewController, manager: ArtistDetailsManager(with: ServiceProvider.shared), artist: artist)
        return artistDetailsViewController
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.didLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //TableView Header = 1/3 view
        tableView.layoutTableViewHeader(with: view.frame.height/3)
    }
}

extension ArtistDetailsViewController: UITableViewDataSource {
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRows() ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.songCellIdentifier.rawValue, for: indexPath) as? SongTableViewCell else { return UITableViewCell() }
        
        presenter?.configure(cell, at: indexPath.row)
        return cell
    }
}

extension ArtistDetailsViewController: UITableViewDelegate {
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.playSong(in: playerView, at: indexPath.row)
    }
}


extension ArtistDetailsViewController: ArtistDetailsViewProtocol {
    func configure(with modelUI: DetailsModelUI) {
        artistDetailsHeaderView.configure(withCoverUrl: modelUI.albumCoverUrl, albumTitle: modelUI.albumTitle, artistName: modelUI.artistName)
        tableView.reloadData()
    }
    
    func selectRow(at index: Int) {
        tableView.selectRow(at: IndexPath(row: index, section: .zero), animated: true, scrollPosition: .none)
    }
}

extension ArtistDetailsViewController: PlayerViewDelegate {
    func nextSong() {
        presenter?.nextSong(in: playerView)
    }
}

extension ArtistDetailsViewController {
    // MARK: Private methods
    private func setupUI() {
        // Customize navigation bar
        navigationController?.navigationBar.isTranslucent = false
        title = "album".localized
        tableView.tableFooterView = UIView(frame: .zero)
        
        playerView.delegate = self
    }
}
