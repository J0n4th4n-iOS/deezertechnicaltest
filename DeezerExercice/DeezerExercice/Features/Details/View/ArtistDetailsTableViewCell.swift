//
//  ArtistDetailsTableViewCell.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol SongTableViewCellProtocol {
    func configure(with songTitle: String?)
}

class SongTableViewCell: UITableViewCell {
    @IBOutlet weak var songTitleLabel: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

extension SongTableViewCell: SongTableViewCellProtocol {
    func configure(with songTitle: String?) {
        songTitleLabel.text = songTitle
    }
}

extension SongTableViewCell {
    private func setupUI() {
        // Configure selection style
        let backgroundColorView = UIView()
        backgroundColorView.backgroundColor = .blueApp
        selectedBackgroundView = backgroundColorView
    }
}
