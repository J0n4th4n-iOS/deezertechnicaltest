//
//  ArtistDetailsHeaderView.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 11/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

protocol ArtistDetailsHeaderViewProtocol {
    func configure(withCoverUrl coverUrl: String?, albumTitle: String?, artistName: String?)
}

class ArtistDetailsHeaderView: UIView, NibLoadable {
    // MARK: Outlets
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumTitleLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        loadNibContent()
        setupUI()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNibContent()
        setupUI()
    }
}

extension ArtistDetailsHeaderView: ArtistDetailsHeaderViewProtocol {
    func configure(withCoverUrl coverUrl: String?, albumTitle: String?, artistName: String?) {
        albumImageView.load(from: coverUrl, completionHandler: { [weak self] in
            guard let self = self else { return }
            self.shouldHideDetails(false)
            self.albumTitleLabel.text = albumTitle
            self.artistNameLabel.text = artistName
        })
    }
}

extension ArtistDetailsHeaderView {
    // MARK: Private methods
    func setupUI() {
        backgroundColor = .grayApp
        shouldHideDetails(true)
        
        albumImageView.cornerRadius = 15
        albumImageView.borderWidth = 2
        albumImageView.borderColor = .white
    }
    
    private func shouldHideDetails(_ isHidden: Bool) {
        artistNameLabel.isHidden = isHidden
        albumTitleLabel.isHidden = isHidden
        albumImageView.isHidden = isHidden
    }
}
