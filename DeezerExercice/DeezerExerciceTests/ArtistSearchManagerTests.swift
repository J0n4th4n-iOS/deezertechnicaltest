//
//  SearchManagerTests.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import XCTest
@testable import DeezerExercice

class ArtistSearchManagerTests: XCTestCase {
    func createSUT(forceError: Bool = false) -> ArtistSearchManager {
        let sut = ArtistSearchManager(with: ServiceProviderMock(forceError: forceError))
        return sut
    }
}

extension ArtistSearchManagerTests {
    func test_search_success() {
        // Given
        let sut = createSUT()
        // When
        var isError = false
        let searchExpectation = expectation(description: "search expectation")
        sut.search(type: .artist, text: "") { _ in
            searchExpectation.fulfill()
        } failure: { error in
            isError = true
            searchExpectation.fulfill()
        }

        waitForExpectation()
        // Then
        XCTAssertFalse(isError)
    }
    
    func test_search_failure() {
        // Given
        let sut = createSUT(forceError: true)
        // When
        var isError = false
        let searchExpectation = expectation(description: "search expectation")
        sut.search(type: .artist, text: "") { _ in
            searchExpectation.fulfill()
        } failure: { error in
            isError = true
            searchExpectation.fulfill()
        }

        waitForExpectation()
        // Then
        XCTAssertTrue(isError)
    }
    
    func test_loadNextArtists_success() {
        // Given
        let sut = createSUT()
        // When
        var isError = false
        let loadNextArtistsExpectation = expectation(description: "loadNextArtistsExpectation expectation")
        
        sut.loadNextArtists(url: "nextArtists", success: { _ in
            loadNextArtistsExpectation.fulfill()
        }, failure: { _ in
            isError = true
            loadNextArtistsExpectation.fulfill()
        })

        waitForExpectation()
        // Then
        XCTAssertFalse(isError)
    }
    
    func test_loadNextArtists_failure() {
        // Given
        let sut = createSUT(forceError: true)
        // When
        var isError = false
        let loadNextArtistsExpectation = expectation(description: "loadNextArtistsExpectation expectation")
        sut.loadNextArtists(url: "", success: { _ in
            loadNextArtistsExpectation.fulfill()
        }, failure: { _ in
            isError = true
            loadNextArtistsExpectation.fulfill()
        })

        waitForExpectation()
        // Then
        XCTAssertTrue(isError)
    }
}
