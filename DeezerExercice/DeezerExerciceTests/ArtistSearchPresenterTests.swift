//
//  SearchPresenterTests.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import XCTest
@testable import DeezerExercice

class ArtistSearchPresenterTests: XCTestCase {
    private typealias Package = (sut: ArtistSearchPresenter, view: ArtistSearchViewControllerMock, manager: ArtistSearchManagerMock)
    
    private func createSUT(needData: Bool = false, forceError: Bool = false) -> Package {
        let view = ArtistSearchViewControllerMock()
        let manager = ArtistSearchManagerMock(with: needData, needError: forceError)
        let sut = ArtistSearchPresenter(with: view, manager: manager, workItem: WorkItemMock())
        
        return (sut: sut, view: view, manager: manager)
    }
}

extension ArtistSearchPresenterTests {
    func test_numberOfRows_noData() {
        // Given
        let package = createSUT()
        // When
        package.sut.search(artistName: "artist")
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == .zero)
    }
    
    func test_numberOfRows_data() {
        // Given
        let package = createSUT(needData: true)
        // When
        package.sut.search(artistName: "artist")
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == 1)
    }
    
    func test_clearSearchResults_noInitialData() {
        // Given
        let package = createSUT()
        // When
        package.sut.clearSearchResults()
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == .zero)
    }
    
    func test_clearSearchResults_withInitialData() {
        // Given
        let package = createSUT(needData: true)
        // When
        package.sut.clearSearchResults()
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == .zero)
    }
    
    func test_search_withEmptyResult() {
        // Given
        let package = createSUT()
        // When
        package.sut.search(artistName: "artist")
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == .zero)
        XCTAssertTrue(package.view.isShowBackgroundViewCallStatus.firstCallParam == .some(.empty))
        XCTAssertTrue(package.view.isReloadDataCallStatus.firstCallParam == .none)
        XCTAssertFalse(package.view.isReloadDataCallStatus.isCalled)
    }
    
    func test_search_withResults() {
        // Given
        let package = createSUT(needData: true)
        // When
        package.sut.search(artistName: "artist")
        // Then
        XCTAssertTrue(package.sut.numberOfRows() == 1)
        XCTAssertFalse(package.view.isShowBackgroundViewCallStatus.isCalled)
        XCTAssertTrue(package.view.isReloadDataCallStatus.isCalled)
    }
    
    func test_configureCell_withNoArtist() {
        // Given
        let package = createSUT()
        let cellMock = ArtistCollectionViewCellMock()
        // When
        package.sut.configure(cellMock, at: .zero)
        // Then
        XCTAssertTrue(cellMock.isConfigureCallStatus.isCalled)
        XCTAssertTrue(cellMock.isConfigureCallStatus.firstCallParam == .some(nil))
    }
    
    func test_configureCell_withArtist() {
        // Given
        let package = createSUT(needData: true)
        package.sut.search(artistName: "artist")
        let cellMock = ArtistCollectionViewCellMock()
        // When
        package.sut.configure(cellMock, at: .zero)
        // Then
        XCTAssertTrue(cellMock.isConfigureCallStatus.isCalled)
        XCTAssertFalse(cellMock.isConfigureCallStatus.firstCallParam == .some(nil))
    }
}
