//
//  ServiceProviderMock.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
@testable import DeezerExercice

class ServiceProviderMock: ServiceProviderProtocol {
    // MARK: Properties
    var forceError: Bool = false
    
    init(forceError: Bool) {
        self.forceError = forceError
    }
    
    func request<T>(_ url: String?, completion: @escaping (Result<T>) -> Void) where T : Decodable {
        if forceError {
            completion(.failure(ServiceError.missingData))
        } else {
            var data: Data?
            if url == "nextArtists" {
                data = JSONTools.load(fromFile: "artists.json", inBundle: Bundle(for: ServiceProviderMock.self))
            } else  {
            
            }
            processSuccessResult(with: data, completion: completion)
        }
    }
    
    func request<T>(_ endpoint: Endpoint, completion: @escaping (Result<T>) -> Void) where T : Decodable {
        if forceError {
            completion(.failure(ServiceError.missingData))
        } else {
            var data: Data?
            switch endpoint {
            case .search(searchFilter: let searchFilter, _):
                switch searchFilter {
                case .artist:
                    data = JSONTools.load(fromFile: "artists.json", inBundle: Bundle(for: ServiceProviderMock.self))
                case .album:
                    data = JSONTools.load(fromFile: "albums.json", inBundle: Bundle(for: ServiceProviderMock.self))
                default:
                    break
                }
            }
            processSuccessResult(with: data, completion: completion)
        }
    }
}

extension ServiceProviderMock {
    // MARK: Private methods
    private func processSuccessResult<T>(with data: Data?, completion: @escaping (Result<T>) -> Void) where T : Decodable {
        guard let dataJson = data else {
            completion(.failure(ServiceError.missingData))
            return
        }
        let result: Result<T> = self.deserializeJSON(with: dataJson)
    
        completion(result)
    }
    
    private func deserializeJSON<T: Decodable>(with data: Data?) -> Result<T> {
        guard let data = data else { return .failure(ServiceError.missingData) }

        do {
            let deserializedValue = try JSONDecoder().decode(T.self, from: data)
            return .success(deserializedValue)
        } catch let error {
            return .failure(error)
        }
    }
}
