//
//  ArtistSearchPresenterMock.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
@testable import DeezerExercice

class ArtistSearchPresenterMock: ArtistSearchPresenterProtocol {
    var isNumberOfRowsCallStatus = CallStatus<Never>.none
    var isConfigureCallStatus = CallStatus<(ArtistCollectionViewCellProtocol, Int)>.none
    var isDidSelectItemCallStatus = CallStatus<(Int)>.none
    var isShouldPrefetchItemsCallStatus = CallStatus<([IndexPath])>.none
    var isSearchCallStatus = CallStatus<(String?)>.none
    var isClearSearchResults = CallStatus<Never>.none
    
    func numberOfRows() -> Int {
        isNumberOfRowsCallStatus.iterate()
        return 0
    }
    
    func configure(_ cell: ArtistCollectionViewCellProtocol, at index: Int) {
        isConfigureCallStatus.iterate(with: (cell, index))
    }
    
    func didSelectItem(at index: Int) {
        isDidSelectItemCallStatus.iterate(with: index)
    }
    
    func shouldPrefetchItems(at indexPaths: [IndexPath]) {
        isShouldPrefetchItemsCallStatus.iterate(with: (indexPaths))
    }
    
    func search(artistName: String?) {
        isSearchCallStatus.iterate(with: artistName)
    }
    
    func clearSearchResults() {
        isClearSearchResults.iterate()
    }
    
    
}
