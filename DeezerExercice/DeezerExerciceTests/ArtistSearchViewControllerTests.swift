//
//  SearchViewControllerTests.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import XCTest
@testable import DeezerExercice

class ArtistSearchViewControllerTests: XCTestCase {
    private typealias Package = (sut: ArtistSearchViewController, presenter: ArtistSearchPresenterMock)
    
    private func createSUT() -> Package {
        let presenter = ArtistSearchPresenterMock()
        let sut: ArtistSearchViewController = safeLoad(from: .main)
        _ = sut.view
        sut.presenter = presenter
        
        return Package(sut, presenter)
    }
}

extension ArtistSearchViewControllerTests {
    func test_showBackgroundView_filled() {
        // Given
        let package = createSUT()
        // When
        package.sut.showBackgroundView(for: CollectionViewState.error)
        // Then
        XCTAssertNotNil(package.sut.collectionView.backgroundView)
    }
    
    func test_showBackgroundView_nil() {
        // Given
        let package = createSUT()
        // When
        package.sut.showBackgroundView(for: CollectionViewState.loaded)
        // Then
        XCTAssertNil(package.sut.collectionView.backgroundView)
    }
    
    func test_reloadData_backgroundView_nil() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.showBackgroundView(for: CollectionViewState.loaded)
        // THEN
        XCTAssertNil(package.sut.collectionView.backgroundView)
    }
}
