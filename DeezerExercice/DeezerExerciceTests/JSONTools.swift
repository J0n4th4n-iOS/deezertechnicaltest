//
//  JSONTools.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

enum ExtensionFile: String {
    case json
}

struct JSONTools {
    public static func load(fromFile: String, inBundle bundle: Bundle) -> Data? {
        if fromFile.contains(".json") {
            let file = String(fromFile.split(separator: ".").first ?? "")
            return Data.load(from: bundle, andFile: file, withType: .json, options: .mappedIfSafe)
        } else {
            return Data.load(from: bundle, andFile: fromFile, withType: .json, options: .mappedIfSafe)
        }
    }
}
