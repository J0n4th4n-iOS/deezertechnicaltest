//
//  Data+Extensions.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

extension Data {
    static func load(from bundle: Bundle,
                     andFile file: String,
                     withType fileType: ExtensionFile,
                     options: Data.ReadingOptions = []) -> Data? {
        guard
            let path = bundle.path(forResource: file, ofType: fileType.rawValue),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: options)
        else {
            return nil
        }
        return data
    }
}
