//
//  CallStatus.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation

public enum CallStatus<T>: Equatable {
    case none
    case called(iterations: Int, results: [T])
    
    public var isCalled: Bool {
        return self != .none
    }
    
    public mutating func iterate() {
        var newIterations = 1
        if case .called(let iterations, _) = self {
            newIterations = iterations + 1
        }
        self = .called(iterations: newIterations, results: [])
    }
    
    public mutating func iterate(with param: T) {
        var newIterations = 1
        var newResults: [T] = []
        if case .called(let iterations, let results) = self {
            newIterations = iterations + 1
            newResults = results
        }
        newResults.append(param)
        self = .called(iterations: newIterations, results: newResults)
    }
    
    public var firstCallParam: T? {
        switch self {
        case .none:
            return nil
        case .called(_, let results):
            return results.first
        }
    }
    
    public var lastCallParam: T? {
        switch self {
        case .none:
            return nil
        case .called(_, let results):
            return results.last
        }
    }
    
    public static func == (lhs: CallStatus, rhs: CallStatus) -> Bool {
        switch (lhs, rhs) {
        case (.none, .none):
            return true
        case (.called, .called):
            return true
        default:
            return false
        }
    }
}
