//
//  ArtistSearchViewControllerMock.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import UIKit
@testable import DeezerExercice


class ArtistSearchViewControllerMock: UIViewController, ArtistSearchViewProtocol {
    var isShowBackgroundViewCallStatus = CallStatus<CollectionViewState>.none
    var isReloadDataCallStatus = CallStatus<[IndexPath]?>.none
    
    func showBackgroundView(for state: CollectionViewState) {
        isShowBackgroundViewCallStatus.iterate(with: state)
    }
    
    func reloadData(at indexPaths: [IndexPath]?) {
        isReloadDataCallStatus.iterate(with: indexPaths)
    }
    
}
