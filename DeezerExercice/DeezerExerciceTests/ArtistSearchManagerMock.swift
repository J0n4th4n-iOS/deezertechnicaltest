//
//  SearchManagerMock.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
@testable import DeezerExercice

class ArtistSearchManagerMock: ArtistSearchManagerProtocol {
    var needFakeData: Bool?
    var needError: Bool?
    
    init(with fakeData: Bool = false, needError: Bool = false ) {
        self.needFakeData = fakeData
        self.needError = needError
    }
    
    var isSearchCallStatus = CallStatus<(SearchFilter, (Artists) -> Void, (Error) -> Void)>.none
    var isLoadNextArtistsCallStatus = CallStatus<(String?, (Artists) -> Void, (Error) -> Void)>.none
    var isCancelCallStatus = CallStatus<Never>.none
    
    func search(type: SearchFilter, text: String, success: @escaping (Artists) -> Void, failure: @escaping (Error) -> Void) {
        isSearchCallStatus.iterate(with: (type, success, failure))
        
        if needFakeData ?? false {
            let artists = Artists()
            artists.total = 1
            artists.next = ""
            let artist = Artist()
            artist.artistIdentifier = 1
            artist.artistName = "Artist"
            artists.items = [artist]
            success(artists)
        } else if needError ?? false {
            failure(ServiceError.missingData)
        } else {
            // Empty result
            success(Artists())
        }
    }
    
    func loadNextArtists(url: String?, success: @escaping (Artists) -> Void, failure: @escaping (Error) -> Void) {
        isLoadNextArtistsCallStatus.iterate(with: (url, success, failure))
    }
    
    func cancel() {
        isCancelCallStatus.iterate()
    }
}
