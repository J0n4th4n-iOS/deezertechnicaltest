//
//  WorkItemMock.swift
//  DeezerExercice
//
//  Created by Jonathan BACQUEY on 16/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
@testable import DeezerExercice

class WorkItemMock: WorkItemProtocol {
    var isPerformCallStatus = CallStatus<(() -> Void)>.none
    
    func perform(_ block: @escaping () -> Void) {
        isPerformCallStatus.iterate(with: block)
        DispatchQueue.main.guaranteeMainThread(work: block)
    }
}
