//
//  XCTestCase+extensions.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
import XCTest
@testable import DeezerExercice

extension XCTestCase {
    func waitForExpectation(duration: TimeInterval = 10) {
        waitForExpectations(timeout: duration) { error in
            if let error = error {
                XCTFail("Expectation Error : \(error)")
            }
        }
    }
    
    func safeLoad<T>(from storyboardIdentifier: StoryboardIdentifier) -> T
    where T: UIViewController {
        let storyboard = UIStoryboard(name: storyboardIdentifier.rawValue, bundle:Bundle(for: T.self))
        let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: T.self))
        
        guard let vc = viewController as? T else {
            fatalError("Cannot load view controller from storyboard.")
        }
        return vc
    }
}
