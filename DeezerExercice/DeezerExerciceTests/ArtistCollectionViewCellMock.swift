//
//  ArtistTableViewCellMock.swift
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 16/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

import Foundation
@testable import DeezerExercice

class ArtistCollectionViewCellMock: ArtistCollectionViewCellProtocol {
    var isConfigureCallStatus = CallStatus<Artist?>.none
    
    func configure(with artist: Artist?) {
        isConfigureCallStatus.iterate(with: artist)
    }
    
    
}
