//
//  DeezerExerciceTests-Bridging-Header.h
//  DeezerExerciceTests
//
//  Created by Jonathan BACQUEY on 15/10/2021.
//  Copyright © 2021 Deezer. All rights reserved.
//

#ifndef DeezerExerciceTests_Bridging_Header_h
#define DeezerExerciceTests_Bridging_Header_h

#import "AppDelegate.h"
#import "ArtistSearchViewController.h"
#import "ArtistCollectionViewCell.h"
#import "Constants.h"

#endif /* DeezerExerciceTests_Bridging_Header_h */
